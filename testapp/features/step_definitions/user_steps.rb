Given /^admin user exists$/ do
  @user = ::FactoryGirl.create :admin_user, name: 'Test Admin'
end

Given /^I am logged in$/ do
  login_as(@user)
end
