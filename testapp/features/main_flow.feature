Feature: Main flow

  @javascript
  Scenario: Admin logs into the system and creates new page
    Given admin user exists
    Given I am on login page
    When I fill in "user[email]" field with "testman@bla.com"
    And I fill in "user[password]" field with "password"
    And I wait for 2 seconds
    And I click on "Log in"
    And I wait for 2 seconds
    And I go to new_page
    And I fill in "page[title]" field with "Bla bla bla page"
    And I fill in "page[text]" field with "Foo baz bar page conent goes here"
    And I wait for 2 seconds
    And I click on "Save"
    And I wait for 3 seconds
    Then I should see text 'Page "Bla bla bla page" was successfully saved'
