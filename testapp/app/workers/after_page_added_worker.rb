class AfterPageAddedWorker
  include Sidekiq::Worker

  def perform(page_id)
    page = ::Page.find(page_id)
    logger.info "Doing hard work with page \"#{page.title}\"...."
    sleep 20
  end
end
