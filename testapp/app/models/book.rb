class Book < ActiveRecord::Base
  has_many :pages
  has_and_belongs_to_many :authors

  def count_pages
    self.pages.count
  end
end
