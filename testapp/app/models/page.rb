class Page < ::ActiveRecord::Base
  belongs_to :book

  validates :title, presence: true
  validates :text, presence: { message: I18n.t('webui.pages.errors.text_empty', username: 'Vasya') }

  before_create :log_model_creation

  def log_model_creation
    logger.info "Model #{self.inspect} is going to be saved!"
  end
end
