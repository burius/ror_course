class HomeController < ApplicationController

  def index
    logger.info "Index action called!"
    @result = 1 + 2
    @arr = [1, 2, 3, :another_symbol, 'bla', :yet_another_symbol, 'ggg', :bla]
  end

  def page
    logger.info "Page method called, params: #{params}"
    @pages_count = ::Page.count
    @page = ::Page.where({id: params[:page_id]}).first
  end

end
