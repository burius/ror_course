class PagesController < ApplicationController
  before_action :init_page, only: [:new, :create]
  before_action :authenticate_user!

  def new
    session[:bla] = 'bla bla'
    render :new
  end

  def create
    sleep 1
    @page.attributes = page_params
    respond_to do |format|
      if @page.save
        do_heavy_lifting(@page)
        format.html { redirect_to :pages }
        format.js   { }
      else
        logger.info "Page errors: #{@page.errors.full_messages}"
        format.html { render :new }
        format.js   { render :errors }
      end
    end
  end

  def index
    @pages = ::Page.accessible_by(current_ability)
  end

  private

  def page_params
    params.require(:page).permit(:title, :text)
  end

  def init_page
    @page ||= ::Page.new
  end

  def do_heavy_lifting(page)
    ::AfterPageAddedWorker.perform_async(page.id)
  end

end
