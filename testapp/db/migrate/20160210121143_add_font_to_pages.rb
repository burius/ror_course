class AddFontToPages < ActiveRecord::Migration
  def change
    add_column :pages, :font, :string, limit: 50, null: false, default: ''
  end
end
