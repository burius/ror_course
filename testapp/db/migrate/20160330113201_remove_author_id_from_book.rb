class RemoveAuthorIdFromBook < ActiveRecord::Migration
  def up
    remove_column :books, :author_id
  end
  def down
    # do nothing
  end
end
