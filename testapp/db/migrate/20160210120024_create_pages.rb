class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.string :title, limit: 100, null: false, default: ''
      t.text :text, null: false, default: ''
      t.integer :author_id
      t.integer :number, null: false, default: 1
    end
  end
end
