class CreateBooks < ActiveRecord::Migration
  def change
    create_table :books do |t|
      t.string :title, limit: 100, null: false, default: ''
      t.integer :author_id, null: false
      t.timestamps null: false
    end
  end
end
