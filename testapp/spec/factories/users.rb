FactoryGirl.define do
  factory :user do
    name 'Test Testovich'
    email 'testman@bla.com'
    password 'password'
    password_confirmation 'password'
  end
  factory :admin_user, parent: :user do
    role 'admin'
  end
end
