FactoryGirl.define do
  factory :page do
    title 'Test Page'
    text 'Text bla bla bla'
    font 'Times New Roman'
  end
end
