require 'rails_helper'

describe Book do

  subject { ::FactoryGirl.create :book, title: 'Another test book' }

  context '#count_pages' do
    
    it "should return 0 for empty book" do
      expect(subject.count_pages).to equal(0)
    end

    it 'should return pages number for book with pages' do
      page1 = ::FactoryGirl.create :page, title: 'Page1'
      page2 = ::FactoryGirl.create :page, title: 'Page2'
      subject.pages << page1
      subject.pages << page2
      expect(subject.count_pages).to equal(2)
    end
  end
  
end
