# Ruby on Rails Programming Course, NaUKMA 2016

### Оголошення
* Good luck to everyone working on projects at the moment! Go-go-go!
* Розгляд/захист проектів => 20.04.2016 11:40 - 13:00

### Матеріали Курсу
* [Робочий тематичний план (GoogleDocs)](https://docs.google.com/document/d/1AtAyf9jQ2VjXB85re-u6UldUkn_LcMR-MwPDN07hQnQ/edit?usp=sharing)
* [Lecture screencasts (FTP)](ftp://94.45.82.58/) login/password: student/Students
* Скрінкаст лекції від 23.03.2016, 720p (FTP) [частина 1](ftp://94.45.82.58/720p/Lecture_Rails_29_03_2016_part1_720p.mov), [частина 2](ftp://94.45.82.58/720p/Lecture_Rails_29_03_2016_part2_720p.mov)

### Контрольний Проект
* [Вимоги до проекта](https://bitbucket.org/burius/ror_course/src/5264c443d55b996bd92d75e19144f758c403560b/students/?at=master) - див. README
* Найпізніший час останнього коміту в проект = 20.04.2016 11:39:59
* Найпізніший час розгляду/захисту проектів  = 20.04.2016 11:40 - 13:00

### Useful Links
* [Ruby style guide](https://github.com/bbatsov/ruby-style-guide) by bbatsov
* [Official Ruby documentation (core API)](http://ruby-doc.org/core-2.3.0/)
* [Rails API documentation](http://api.rubyonrails.org/)
* [Official Rails guides](http://guides.rubyonrails.org/)
* [Bootstrap CSS/Javascript framework](http://getbootstrap.com/)
* [JQuery-ujs Wiki](https://github.com/rails/jquery-ujs/wiki)
* [CoffeScript](http://coffeescript.org/)

### Legendary Rubyists on the Web
* [matz](https://twitter.com/yukihiro_matz) - Yukihiro Matsumoto, author of Ruby. MINASWAN (Matz is Nice And So We Are Nice)
* [dhh](https://twitter.com/dhh) - David Heinemeier Hansson, initial author of Ruby on Rails
* [tenderlove](https://twitter.com/tenderlove) - Aaron Patterson, Rails core contributor and a very nice guy
* [wycatz](https://twitter.com/wycats) - Yehuda Katz, initial author of Merb; ruby, javascript and rust hacker
* [josevalim](https://twitter.com/josevalim) - José Valim, one of the top Rails contributors, author of Elixir language
