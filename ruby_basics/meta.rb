class M
  
  def method_missing(method_name, *args)
    puts "missing method: #{method_name}"
    if method_name =~ /get_[a-z0-9]+/
      1
    else
      raise NoMethodError.new('No such method')
    end
  end

end
