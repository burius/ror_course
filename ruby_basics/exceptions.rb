class MyException < StandardError

end

class A
  
  def bla
    raise MyException.new('my custom error message')
    a = nil
    a + 2
  rescue NoMethodError => e
    puts "Exception occured: #{e.message} #{e.backtrace.join("\n")}"
  rescue => e
    puts "Generic exception occured: #{e.message} #{e.backtrace.join("\n")}"
  ensure
    puts "finally!"
    nil
  end

  def foo
    self.bla || 0
  end

end
