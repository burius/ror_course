module M
  
  def module_bla
    puts "method from module!"
  end

end

module AnotherModule

  def ggg
    @m = 1
  end

end

module A
  module A1

    class AaaBlaBla

      @@v = 1

      def initialize(x)
        a = 1
        @b = 1
        @x = x
        puts 'Called every time when new instance is initiated'
      end

      def self.ggg
        5
      end

      def inc_b
        @b += 1
      end

      def inc_v
        @@v += 1
      end

      def method_missing(method_name)
        puts "Add method! #{method_name}"
      end

      def inspect
        puts "@b: #{@b}, @@v: #{@@v}"
      end
      
      def bla
        puts "@b: #{@b}"
        name = readline.chomp
        puts 'bla'
      end

      def foo_and_bar(a, b)
        puts "params: #{a} #{b}"
        1
      end

      private
      def private_method
        5
        @b += 1
      end

    end

  end
end

class B < A::A1::AaaBlaBla
  include M
  include AnotherModule
  
  def bla
    super
    puts 'bla bla bla'
  end

end
