# Triangle Project Code.

# Triangle analyzes the lengths of the sides of a triangle
# (represented by a, b and c) and returns the type of triangle.
#
# It returns:
#   :equilateral  if all sides are equal
#   :isosceles    if exactly 2 sides are equal
#   :scalene      if no sides are equal
#
# The tests for this method can be found in
#   about_triangle_project.rb
# and
#   about_triangle_project_2.rb
#

def validate(a, b, c)
  *args = a, b, c
  raise TriangleError unless args.select { |arg| arg <= 0 }.empty?
  s1, s2, s3 = args.sort
  raise TriangleError if s1 + s2 <= s3
end

def triangle(a, b, c)
  validate(a, b, c)
  return :equilateral if a == b && b == c
  return :isosceles if a == b || b == c || a == c
  :scalene
end

# Error class used in part 2.  No need to change this code.
class TriangleError < StandardError
end
